all: formation-git-2.pdf

formation-git-2.pdf: src/formation-git-2.tex src/img/title-banner.png src/img/repository.png src/img/history.jpg src/img/commit.png src/img/branch.jpg src/img/branches.png src/img/merge.png src/img/master.jpg src/img/feature.jpg src/img/bugfix.jpg
	xelatex src/formation-git-2.tex
	xelatex src/formation-git-2.tex

src/img/title-banner.png:
	rsvg-convert --width 512 --output src/img/title-banner.png --keep-aspect-ratio src/img/title-banner.svg

src/img/branches.png:
	rsvg-convert --width 512 --output src/img/branches.png --keep-aspect-ratio src/img/branches.svg

src/img/merge.png:
	rsvg-convert --width 512 --output src/img/merge.png --keep-aspect-ratio src/img/merge.svg

clean:
	rm -f *.aux *.log *.nav *.out *.snm *.toc
	rm -f src/img/title-banner.png
	rm -f src/img/branches.png
	rm -f src/img/merge.png
